package vn.tinylab.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bol")
public class BOL  implements Serializable  {
		 
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="bol_no")
	private String bolNo;

	public String getBlNo() {
		return bolNo;
	}

	public void setBlNo(String bolNo) {
		this.bolNo = bolNo;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}
	@Column(name="user_id")
	private Long userId;
}
