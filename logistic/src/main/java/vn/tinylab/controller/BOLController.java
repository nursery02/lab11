package vn.tinylab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import vn.tinylab.model.entity.BOL;
import vn.tinylab.model.dto.*;
import vn.tinylab.model.response.ResponseModel;
import vn.tinylab.service.Impl.BOLService;


@RestController
@RequestMapping("/bol")
public class BOLController {
	
	@Autowired
	private EurekaClient client;
	
	@Autowired
	private RestTemplateBuilder restTemlateBuilder;
	
	@Autowired
	private BOLService orderService;

    @GetMapping("/test")
    public ResponseModel one() {  
    	InstanceInfo ist = client.getNextServerFromEureka("user-service", false);
		String url = ist.getHomePageUrl();
		
		RestTemplate tmpl = restTemlateBuilder.build();
		ResponseEntity<?> ru = tmpl.exchange(url + "/users/instance-name" , HttpMethod.GET, null, new ResponseModel<String>().getClass());    		
    	return new ResponseModel(true, ((ResponseModel<String>)ru.getBody()).getData());    	
    }
    
 	@PostMapping("")
	public ResponseEntity<BOL> createUser(@RequestBody BOL bol) throws JsonProcessingException {
 		BOL dt = orderService.createNew(bol);
        return ResponseEntity.ok(dt);
    }    
 	
    @GetMapping("/{id}")
    public ResponseModel one(@PathVariable Long id) {    
    	try {
    		BOL odr = orderService.findById(id);
    		
    		InstanceInfo ist = client.getNextServerFromEureka("user-service", false);
    		String url = ist.getHomePageUrl();
    		
    		RestTemplate tmpl = restTemlateBuilder.build();
    		ResponseEntity<?> ru = tmpl.exchange(url + "/users/" + odr.getId() , HttpMethod.GET, null, new ResponseModel<UserDTO>().getClass()); 
    		
    		//Cast data from response model to object AppUser
    		ObjectMapper mapper = new ObjectMapper();    		
    		ResponseModel<UserDTO> responseModel = mapper.convertValue(ru.getBody(), new TypeReference<ResponseModel<UserDTO>>(){});
    		
    		BOLDTO lddto = new BOLDTO();
    		lddto.setBol(odr);
    		lddto.setNote(url);
			lddto.setUser(responseModel.getData());
        	return new ResponseModel(true, lddto);
    	}catch(Exception e) {
        	return new ResponseModel(false, null, e.getMessage());
    	}
    	
    }
    
}
