package vn.tinylab.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.tinylab.model.entity.BOL;
import vn.tinylab.repository.BOLRepository;
import vn.tinylab.service.IBOLService;

@Service
public class BOLService implements IBOLService {

	@Autowired
	BOLRepository bolRepo;
	
	@Override
	public BOL createNew(BOL bol) {
		bolRepo.save(bol);
		return bol;
	}


	@Override
	public BOL findById(Long id) {
		return bolRepo.findById(id).orElseThrow();
	}


}
